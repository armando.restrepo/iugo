package com.yuxiglobal.iugo.data.CargoRequest

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by AresDev on 12/6/17.
 */
import android.widget.TextView

import com.couchbase.lite.DataSource
import com.couchbase.lite.Database
import com.couchbase.lite.Expression
import com.couchbase.lite.LiveQuery
import com.couchbase.lite.Query
import com.couchbase.lite.Result
import com.couchbase.lite.SelectResult
import com.yuxiglobal.iugo.R
import com.yuxiglobal.iugo.ui.ListItemSelectedListener
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * Created by hideki on 6/26/17.
 */

class CargoRequestLiveAdapter(private val context: Context, private val db: Database, private val layoutResource: Int, private val listItemSelectedListener: ListItemSelectedListener?) : RecyclerView.Adapter<CargoRequestLiveAdapter.ViewHolder>() {
    private val query: LiveQuery
    private var itemCount: Int = 0;
    private val results: ArrayList<String>  = ArrayList<String>()
    init {

        if (db == null) throw IllegalArgumentException()

        this.query = query()
        this.query.addChangeListener { change ->
            results.clear()
            val rs = change.rows
            var result: Result?=null
            while ({ result = rs.next(); result }() != null) {
                val id = result!!.getString(0)
                results.add(id)
            }
            notifyDataSetChanged()
        }
        this.query.run()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val id = results[position]
        val cargoRequestDocument = db.getDocument(id)
        with(holder){
            mSource.text = cargoRequestDocument.getString("source")
            mDestination.text = cargoRequestDocument.getString("destination")
            mAsignDate.text = cargoRequestDocument.getString("assignmentDate")
            mTruckType.text = cargoRequestDocument.getDictionary("truckType").getString("name")
            mTrucksNumber.text = cargoRequestDocument.getString("source")
            mFleet.text = cargoRequestDocument.getString("fleet")
            mCompany.text = cargoRequestDocument.getDictionary("company").getString("name")
            mDescription.text = cargoRequestDocument.getString("description")
            itemView.onClick {
                listItemSelectedListener?.onListItemSelected(cargoRequestDocument.id)
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(layoutResource, parent, false)
        return ViewHolder(view)
    }

    private fun query(): LiveQuery {
        return Query.select(SelectResult.expression(Expression.meta().id))
                .from(DataSource.database(db))
                .where(Expression.property("type").equalTo("cargo-request"))
                        //.and(Expression.property("taskList.id").equalTo(listID)))
                .toLive()
    }

    override fun getItemCount(): Int {
        return  results.count()
    }

    companion object {
        private val TAG = CargoRequestLiveAdapter::class.java.simpleName
    }

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val mSource: TextView = mView.findViewById<TextView>(R.id.lblSource)
        val mDestination: TextView = mView.findViewById<TextView>(R.id.lblDestination) as TextView
        val mCompany: TextView = mView.findViewById<TextView>(R.id.lblCompany) as TextView
        val mTrucksNumber: TextView = mView.findViewById<TextView>(R.id.lblTrucksNumber) as TextView
        val mFleet: TextView = mView.findViewById<TextView>(R.id.lblFleet) as TextView
        val mTruckType: TextView = mView.findViewById<TextView>(R.id.lblTruckType) as TextView
        val mAsignDate: TextView = mView.findViewById<TextView>(R.id.lblAsignDate) as TextView
        val mDescription: TextView = mView.findViewById<TextView>(R.id.lblDescription) as TextView

//        override fun toString(): String {
//            return super.toString() + " '" + mContentView.text + "'"
//        }
    }
}
