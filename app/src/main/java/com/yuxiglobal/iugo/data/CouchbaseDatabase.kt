package com.yuxiglobal.iugo.data

import android.content.ContentValues.TAG
import android.content.Context
import android.widget.Toast
import com.couchbase.lite.*
import com.yuxiglobal.iugo.utils.SingletonHolder
import java.net.URI
import java.net.URISyntaxException
import java.util.*

/**
 * Created by AresDev on 12/4/17.
 */
class CouchbaseDatabase private constructor(context: Context): ReplicatorChangeListener{

    companion object : SingletonHolder<CouchbaseDatabase, Context>(::CouchbaseDatabase)

    private val dbName = "iugo-db"
    private var database: Database? = null
    private var replicator: Replicator? = null
    private val syncEnabled = true
    private val syncUrl: String = "blip://iugo.cloudapp.net:5984/iugo-db/"

    var applicationContext: Context? = null

    fun getDatabase(): Database {
        return database!!
    }


    init {
        applicationContext = context
    }

    fun startSession(username: String? = null, password: String? = null) {

        openDatabase()
        startReplication(username, password)
    }

    private fun openDatabase() {
        val config = DatabaseConfiguration(applicationContext)
        config.conflictResolver = getConflictResolver()
        try {
            database = Database(dbName, config)
        } catch (e: CouchbaseLiteException) {
            Log.e(TAG, "Failed to create Database instance: %s - %s", e, dbName, config)
            // TODO: error handling
        }

    }

    private fun getConflictResolver(): ConflictResolver {
        /**
         * Example: Conflict resolver that merges Mine and Their document.
         */
        return ConflictResolver { conflict ->
            val mine = conflict.mine
            val theirs = conflict.theirs

            val resolved = Document()
            val changed = HashSet<String>()

            // copy all data from theirs document
            for (key in theirs) {
                resolved.setObject(key, theirs.getObject(key))
                changed.add(key)
            }

            // copy all data from mine which are not in mine document
            for (key in mine) {
                if (!changed.contains(key))
                    resolved.setObject(key, mine.getObject(key))
            }

            Log.e(TAG, "ConflictResolver.resolve() resolved -> %s", resolved.toMap())

            resolved
        }
    }

    private fun closeDatabase() {
        if (database != null) {
            try {
                database!!.close()
            } catch (e: CouchbaseLiteException) {
                Log.e(TAG, "Failed to close Database", e)
                // TODO: error handling
            }

        }
    }

    // -------------------------
    // Replicator operation
    // -------------------------
    private fun startReplication(username: String? = null, password: String? = null) {
        if (!syncEnabled) return

        val uri: URI
        try {
            uri = URI(syncUrl)
        } catch (e: URISyntaxException) {
            Log.e(TAG, "Failed parse URI: %s", e, syncUrl)
            return
        }

        val config = ReplicatorConfiguration(database, uri)
        config.replicatorType = ReplicatorConfiguration.ReplicatorType.PUSH_AND_PULL
        config.isContinuous = true

        // authentication
        if (username != null && password != null)
            config.authenticator = BasicAuthenticator(username, password)

        replicator = Replicator(config)
        replicator?.addChangeListener(this)
        replicator?.start()
    }

    private fun stopReplication() {
        if (!syncEnabled) return
        replicator?.stop()
    }


    // --------------------------------------------------
    // ReplicatorChangeListener implementation
    // --------------------------------------------------
//    override fun changed(replicator: Replicator, status: Replicator.Status, error: CouchbaseLiteException?) {
//        Log.i(TAG, "[Todo] Replicator: status -> %s, error -> %s", status, error)
//        if (error != null && error.code == 401) {
//            Toast.makeText(CONTEXT, "Authentication Error: Your username or password is not correct.", Toast.LENGTH_LONG).show()
//            //logout()
//        }
//    }

    override fun changed(change: ReplicatorChange?) {
        Log.i(TAG, "[IUGO] Replicator: status -> %s", change?.status)
        if(change?.status?.error !=  null && change?.status?.error?.code == 401){
            Toast.makeText(applicationContext, "Authentication Error: Your username or password is not correct.", Toast.LENGTH_LONG).show()
        }
    }

    // create task
    private fun createUser(username: String) {
        val random = Random().nextInt(100).toString()
        val docId = random + "." + username
        val doc = Document(docId)
        doc.setString("type", "user")
        doc.setString("username", username)
        val taskListInfo = HashMap<String, Any>()
        taskListInfo.put("id", random)
        //doc.setObject("taskList", taskListInfo)
        try {
            database?.save(doc)
        } catch (e: CouchbaseLiteException) {
            Log.e(TAG, "Failed to save the doc - %s", e, doc)
            //TODO: Error handling
        }

    }




}