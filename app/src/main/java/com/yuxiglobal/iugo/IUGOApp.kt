package com.yuxiglobal.iugo

import android.app.Application
import com.yuxiglobal.iugo.data.CouchbaseDatabase

/**
 * Created by AresDev on 12/4/17.
 */
class IUGOApp: Application() {
    override fun onCreate() {
        super.onCreate()
        CouchbaseDatabase.getInstance(this).startSession("mobile","iugoDBASE")
    }
}