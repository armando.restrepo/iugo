package com.yuxiglobal.iugo.notifications

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

/**
 * Created by AresDev on 12/3/17.
 */
class InstanceIDService : FirebaseInstanceIdService() {
    companion object {
        private val REG_TOKEN = "REG_TOKEN"
    }

    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d(REG_TOKEN, "Refreshed token: " + refreshedToken!!)

        //val prefs = PreferenceHelper.defaultPrefs(applicationContext)
        //prefs[Constants.PREF_TOKEN] = refreshedToken
    }
}