package com.yuxiglobal.iugo.notifications

import android.app.NotificationManager
import android.content.ContentValues.TAG
import android.content.Context
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.yuxiglobal.iugo.R

/**
 * Created by AresDev on 12/3/17.
 */
class MessagingService : FirebaseMessagingService() {
    companion object {
        private val MSG_SERVICE = "MSG_SERVICE"
    }

    // Get a reference to the LocalBroadcastManager
    //val localBroadcastManager = null

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        if (remoteMessage!!.data.size > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            sendNotification(remoteMessage!!.data["title"]!!, remoteMessage!!.data["message"]!!)

            //        val prefs = PreferenceHelper.defaultPrefs(this)
//        var count = prefs.getInt(Constants.PREF_NOTIFICATION_COUNT,0)
//        prefs[Constants.PREF_NOTIFICATION_COUNT] = count + 1
//
//        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.BROADCAST_DEFAULT_NOTIFICATION_ARRIVED))
        }


    }

    private fun sendNotification(title: String, messageBody: String) {
        /*val intent = Intent(this, MainActivity::class.java)
        intent.action = getString(R.string.GO_TO_ALERTS_TAB)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 *//* Request code *//*, intent, PendingIntent.FLAG_ONE_SHOT)
*/
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                //.setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(System.currentTimeMillis().toInt(), notificationBuilder.build())
    }
}