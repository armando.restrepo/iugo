package com.yuxiglobal.iugo.ui.fragments.liningup

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.yuxiglobal.iugo.R
import com.yuxiglobal.iugo.data.CargoRequest.CargoRequestLiveAdapter
import com.yuxiglobal.iugo.data.CouchbaseDatabase
import com.yuxiglobal.iugo.ui.ListItemSelectedListener
import com.yuxiglobal.iugo.ui.activities.MainActivity
import com.yuxiglobal.iugo.ui.activities.TurnActivity
import kotlinx.android.synthetic.main.fragment_lining_up.*
import org.jetbrains.anko.startActivity

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LiningUpFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LiningUpFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LiningUpFragment : Fragment(), ListItemSelectedListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_lining_up, container, false)
        val recycler = view.findViewById<RecyclerView>(R.id.liningUpRecyclerView)
        recycler.adapter = CargoRequestLiveAdapter(activity, CouchbaseDatabase.getInstance(context).getDatabase(), R.layout.listitem_cargo_request, this)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //var doc = CouchbaseDatabase.getInstance(context).getDatabase().getDocument("7aeb4547995b8c5c0ff66c1a9be5d1f6")
//        userId.text = "64.prueba"
//        userName.text = doc.getString("task")
//        userType.text = doc.getString("type")
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        /*if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }*/
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onListItemSelected(id: String) {
        startActivity<TurnActivity>("id" to id)
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LiningUpFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): LiningUpFragment {
            val fragment = LiningUpFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
