package com.yuxiglobal.iugo.ui.activities

import android.app.AlertDialog
import android.app.Fragment
import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.yuxiglobal.iugo.R
import com.yuxiglobal.iugo.ui.fragments.home.HomeFragment
import com.yuxiglobal.iugo.ui.fragments.liningup.LiningUpFragment
import com.yuxiglobal.iugo.ui.fragments.liningup.TurnsFragment
import com.yuxiglobal.iugo.ui.fragments.notifications.NotificationsFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert

class MainActivity : AppCompatActivity() {

    //var fragmentClass: Class<*>?

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        handleNavigation(item.itemId)
    }

    private fun handleNavigation(item: Int): Boolean {
        var fragmentType: Class<*>? = null

        when (item) {
            R.id.navigation_home -> {
                //message.setText(R.string.title_home)
                fragmentManager.beginTransaction().replace(R.id.content, HomeFragment.newInstance("1","2") as Fragment).commit()
                return true
            }
            R.id.navigation_dashboard -> {
                //message.setText(R.string.title_dashboard)
                fragmentManager.beginTransaction().replace(R.id.content, LiningUpFragment.newInstance("1","2") as Fragment).commit()
                return true
            }
            R.id.navigation_notifications -> {
                //message.setText(R.string.title_notifications)
                fragmentManager.beginTransaction().replace(R.id.content, NotificationsFragment.newInstance(1) as Fragment).commit()
                return true
            }
            R.id.navigation_turns -> {
                //message.setText(R.string.title_dashboard)
                fragmentManager.beginTransaction().replace(R.id.content, TurnsFragment.newInstance("1","2") as Fragment).commit()
                return true
            }
        }

        return false

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        handleNavigation(R.id.navigation_home)
    }

    override fun onBackPressed() {

        alert("Warning", "Are you sure you want to exit?") {
            positiveButton("Yes") { finish() }
            negativeButton("No") { }
        }.show()
    }
}
