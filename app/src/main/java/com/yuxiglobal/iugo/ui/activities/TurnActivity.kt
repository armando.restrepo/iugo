package com.yuxiglobal.iugo.ui.activities

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import com.couchbase.lite.Database
import com.couchbase.lite.Document
import com.yuxiglobal.iugo.R
import com.yuxiglobal.iugo.data.CouchbaseDatabase
import com.yuxiglobal.iugo.ui.viewobjects.TurnVO
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yuxiglobal.iugo.ui.adapters.NotificationsRecyclerViewAdapter

import kotlinx.android.synthetic.main.activity_turn.*
import kotlinx.android.synthetic.main.content_turn.*

class TurnActivity : AppCompatActivity() {
    private val db: Database = CouchbaseDatabase.getInstance(this).getDatabase()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_turn)
        setSupportActionBar(toolbar)

        val extras = intent.extras
        if (extras != null) {
            val id = extras.getString("id")
            val cargoRequestDocument = db.getDocument(id)
            //The key argument here must match that used in the other activity
            fab.setOnClickListener { view ->
                Snackbar.make(view, cargoRequestDocument.getString("description"), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
            }

            //turnsRecyclerView.adapter = TurnsAdapter(ArrayList<TurnVO>(createTurns(parentId = id)), R.layout.listitem_turns)
        }



    }

    private fun createTurns(numberOfTrucks : Int = 2, parentId : String): List<TurnVO>{
        lateinit var turnsList:MutableList<TurnVO>
        for (i in 1..numberOfTrucks){
            val turn = TurnVO(parentId)
            turnsList.add(turn)
        }

        return turnsList
    }

    inner class TurnsAdapter<T>(private val context:Context, private val layoutResource: Int, private val list: List<T>): RecyclerView.Adapter<TurnsAdapter<T>.ViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(context).inflate(layoutResource, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            with(holder) {

            }
        }

        override fun getItemCount(): Int = list.count()

        inner class ViewHolder(val mView: View):RecyclerView.ViewHolder(mView){

        }
    }

}
