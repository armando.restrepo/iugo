package com.yuxiglobal.iugo.ui.router

import android.content.Context
import cn.nekocode.meepo.Meepo
import cn.nekocode.meepo.annotation.Bundle
import cn.nekocode.meepo.annotation.TargetClass
import com.yuxiglobal.iugo.ui.activities.MainActivity

/**
 * Created by AresDev on 12/11/17.
 */
interface Router {

    companion object {
        val IMPL = Meepo.Builder().build().create(Router::class.java)
    }

//    @TargetClass(LoginActivity::class)
//    fun gotoLogin(context: Context?) {
//        IMPL.gotoLogin(context)
//    }

    @TargetClass(MainActivity::class)
    fun gotoMain(context: Context?) {
        IMPL.gotoMain(context)
    }
}


/*interface UIRouter {

    companion object {
        val IMPL = Meepo.Builder().build().create(UIRouter::class.java)
    }

    @TargetClass(LoginActivity::class)
    fun gotoLogin(context: Context?) {
        IMPL.gotoLogin(context)
    }

    @TargetClass(MainActivity::class)
    fun gotoMain(context: Context?) {
        IMPL.gotoMain(context)
    }

    @TargetClass(ListAssetsActivity::class)
    fun gotoListAssets(context: Context?) {
        IMPL.gotoListAssets(context)
    }

    @TargetClass(ListSitesActivity::class)
    fun gotoListSites(context: Context?) {
        IMPL.gotoListSites(context)
    }

    @TargetClass(ScannerActivity::class)
    fun gotoScanner(context: Context?, @Bundle("siteId") siteId: String) {
        IMPL.gotoScanner(context, siteId)
    }

    @TargetClass(ScannerDetailActivity::class)
    fun gotoScannerDetail(context: Context?, @Bundle("assetId") assetId: String, @Bundle("siteId") siteId: String) {
        IMPL.gotoScannerDetail(context, assetId, siteId)
    }

    @TargetClass(MLNotificationsDetailActivity::class)
    fun gotoMLNotificationDetail(context: Context?, @Bundle("MLNotification") notification: Notification) {
        IMPL.gotoMLNotificationDetail(context, notification)
    }

    @TargetClass(BarChartActivity::class)
    fun gotoBarChart(context: Context?) {
        IMPL.gotoBarChart(context)
    }

    @TargetClass(ReportAssetsDetailActivity::class)
    fun gotoReportDetail(context: Context?, @Bundle("assetName") assetName: String, @Bundle("status") status: String, @Bundle("username") username: String, @Bundle("loanTime") loanTime: String, @Bundle("assetStatus") assetStatus: String) {
        IMPL.gotoReportDetail(context, assetName, status, username, loanTime, assetStatus)
    }

    @TargetClass(NewAssetActivity::class)
    fun gotoNewAsset(context: Context?) {
        IMPL.gotoNewAsset(context)
    }

    @TargetClass(PropertiesActivity::class)
    fun gotoProperties(context: Context?) {
        IMPL.gotoProperties(context)
    }

    @TargetClass(CreateUserActivity::class)
    fun gotoCreateUser(context: Context?) {
        IMPL.gotoCreateUser(context)
    }

    @TargetClass(CreateUserActivity::class)
    fun gotoEditUser(context: Context?, @Bundle("userId") userId: String, @Bundle("isEdit") isEdit: Boolean) {
        IMPL.gotoEditUser(context, userId, isEdit)
    }

    @TargetClass(TopUsersActivity::class)
    fun gotoTopUsers(context: Context?, @Bundle("siteId") siteId: String, @Bundle("deviceName") deviceName: String) {
        IMPL.gotoTopUsers(context,siteId,deviceName)
    }

    @TargetClass(FirstLoginActivity::class)
    fun gotoFirstLogin(context: Context?, @Bundle("userId") userId: String) {
        IMPL.gotoFirstLogin(context, userId)
    }

    @TargetClass(ForgotPasswordActivity::class)
    fun gotoForgotPassword(context: Context?) {
        IMPL.gotoForgotPassword(context)
    }

    @TargetClass(ReportSitesActivity::class)
    fun gotoReportSites(context: Context?, @Bundle("siteId") siteId: String, @Bundle("deviceName") deviceName: String, @Bundle("status") status: String, @Bundle("hasResults") hasResults: Boolean) {
        IMPL.gotoReportSites(context, siteId,deviceName,status, hasResults)
    }

    @TargetClass(ReportAssetsNewActivity::class)
    fun gotoReportAssetsNew(context: Context?, @Bundle("siteId") siteId: String, @Bundle("deviceSN") deviceName: String, @Bundle("status") status: String, @Bundle("hasResults") hasResults: Boolean) {
        IMPL.gotoReportAssetsNew(context, siteId, deviceName, status, hasResults)
    }*/