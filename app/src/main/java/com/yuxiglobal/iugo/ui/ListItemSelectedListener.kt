package com.yuxiglobal.iugo.ui

/**
 * Created by AresDev on 12/12/17.
 */
interface ListItemSelectedListener {
    fun onListItemSelected(id : String)
}