package com.yuxiglobal.iugo.ui.viewobjects

/**
 * Created by AresDev on 12/13/17.
 */
data class TurnVO(
        val cargoRequestId: String,
        val driverId: String? = null,
        val truckId:String? = null,
        val accesories: List<String>? = null
) : BaseVO(){}